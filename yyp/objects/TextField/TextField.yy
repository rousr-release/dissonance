{
    "id": "3950ef39-de9f-4976-9240-24ab521f57b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TextField",
    "eventList": [
        {
            "id": "494db9f3-cafd-4879-bd5c-8228e9cef9b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3950ef39-de9f-4976-9240-24ab521f57b7"
        },
        {
            "id": "e9fd36e2-0aad-4354-b1f5-9ee46bb59d7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3950ef39-de9f-4976-9240-24ab521f57b7"
        },
        {
            "id": "0b649dcf-1417-4761-b572-f862e4266f78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3950ef39-de9f-4976-9240-24ab521f57b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "909ae02d-c306-43b4-a1db-f06746d258b4",
    "visible": true
}