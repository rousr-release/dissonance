{
    "id": "d115a3e2-c7d1-41cb-8a2d-b545afabdbae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "CheckBox",
    "eventList": [
        {
            "id": "aeb7714a-7cf1-4c28-ba79-ba58d5b6b798",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d115a3e2-c7d1-41cb-8a2d-b545afabdbae"
        },
        {
            "id": "a6869c5c-8e07-4417-bd34-4283a4e1a0f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d115a3e2-c7d1-41cb-8a2d-b545afabdbae"
        },
        {
            "id": "0e1145e3-f086-4e6b-8300-19928d3a15f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d115a3e2-c7d1-41cb-8a2d-b545afabdbae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "909ae02d-c306-43b4-a1db-f06746d258b4",
    "visible": true
}