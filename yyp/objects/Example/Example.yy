{
    "id": "427d4dad-d5b0-4abe-b5eb-a0f8c9f98501",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Example",
    "eventList": [
        {
            "id": "ec7c278a-0af1-4c4e-aa00-9f258d64a1e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "427d4dad-d5b0-4abe-b5eb-a0f8c9f98501"
        },
        {
            "id": "2ee5c1a7-bd0b-4dd9-861f-42e563af9eed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "427d4dad-d5b0-4abe-b5eb-a0f8c9f98501"
        },
        {
            "id": "5d551737-9983-494d-a50d-1cb75913ae10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "427d4dad-d5b0-4abe-b5eb-a0f8c9f98501"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}