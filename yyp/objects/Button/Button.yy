{
    "id": "cf63e51f-de51-42fe-942e-3b4ec2569db8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Button",
    "eventList": [
        {
            "id": "8bac0499-4cb0-4e91-aa07-ec6d3cc9903b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "9d0fa027-ca3b-4eda-87da-9469723a400f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "5fe67c43-1415-42e2-b5ca-723f92ab7972",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "e4f01883-0c56-455c-9245-d1cb35825ae7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "b2a91c53-4720-421d-ae43-56811288fcee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "e3480af3-8dc9-4224-98cd-ffa597fbfc7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "0ffca0e1-2f8c-4d85-adfa-5d3cb830ac28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
    "visible": true
}