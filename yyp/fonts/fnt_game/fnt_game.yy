{
    "id": "b16728c9-da48-461d-8c3e-322fc7b82179",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_game",
    "AntiAlias": 0,
    "TTFName": "${project_dir}\\fonts\\fnt_game\\04b24.TTF",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "04b_24",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b67d02b0-a585-4e58-8582-61fe44f2ea20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 34,
                "y": 92
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d71baa38-ad09-4a01-ac21-dfc1efe7fc10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 108,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "64bac49b-951b-47a6-a5f3-5868ca883b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 56
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a9f34fa3-8067-4137-8607-cf76d458afda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 56
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a960e303-cbf7-4cb9-a61a-844029a72573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e4c9b442-ba1e-49b9-aba3-8c69453a516e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 56
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "318f51f1-0fc3-46dd-81d3-6f1219b714a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "917a6d0e-c02b-455e-b878-4f05782a5eda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 104,
                "y": 92
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2916ba60-3ad1-499a-93a7-a9b41babd129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 76,
                "y": 92
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7d633672-9e8b-49bc-88ef-4335dcd3cf9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 70,
                "y": 92
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "68e09253-e543-4a2d-8355-d10327066ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 56
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c3b92580-fac9-411f-8c53-13785ed64f3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4958a9a9-2965-4483-9c95-56a5c57b1641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 64,
                "y": 92
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "10965094-ac7c-4f87-822e-4512bb67571d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4729ba1e-8042-4f3c-9e8f-a99c9cd6828e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 100,
                "y": 92
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4c60e53b-b671-428d-90e5-eacc0712e5e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 38
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ec47a254-a69e-43bf-9259-8edff065af69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 38
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f882bba3-4c4b-4064-9b78-ddf1b1a066d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 58,
                "y": 92
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d91aceac-5f67-45d1-92ee-6f1287085f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "46f23010-5fad-4501-beaa-4d3a54606760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7d4cf875-c6e4-4657-89a0-b64f138385a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "be808541-34f6-470b-a2f2-4654c3ceed7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 92
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b70e0063-3585-4671-b0ee-52bd828f7a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9cffde3d-4005-4a4b-b9f9-2a2db380b459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6cf22900-9d1a-4591-bec8-aac0ed3b0a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8ba8238e-5907-43a7-bf7f-5608e76b3e02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "745375a9-0786-4d9e-8179-07d3015d2a66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 96,
                "y": 92
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "89289b2a-fdeb-433c-b76e-11344d987526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 92,
                "y": 92
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "622b7066-5648-4a25-a37e-18af593d3d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ee77d2b1-2c55-4809-9a63-3966c07bdd03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ce0d46a7-b010-42fe-b2fe-9cdbe7435d29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6144c485-6abb-4aed-8e3a-0e8726fc8736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "65a995d2-5cc2-4973-aa01-42eae65c3e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f61d3e1b-f25d-417a-8f8a-7d5ac3fef6c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1930e4c2-a602-4e5f-9daa-fd150545dc26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "970ec068-4751-46c2-84e2-dcbfad743fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1c109461-5543-4cb3-b31a-11265850517d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "dd444a88-4379-4549-9d3f-b34e48f7ec6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b22da97b-73ce-41fb-bb55-616b30147495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 38
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e26495bb-1459-4bf3-8b88-08110991ce0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e843684f-941d-4c6f-ac19-22ba4a187866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 38
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "44860dc5-7223-4c2b-ab8e-a1e2e002f3a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 88,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5ec571d5-8dfe-4f0d-8503-fe101f06c340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 20
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1ee3ffd8-1605-4630-af29-c19a85fafb8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 20
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "35f1e2a5-5b98-4f85-9eb6-aca138708883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "aa0702b2-430a-4a67-9544-621f94148663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "075ec799-d8eb-4ba6-b411-238915027b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ec3c42a4-e6fb-4f1f-a7a2-9927d15f9dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1b08af7f-b1ea-4a11-9e98-1c614f136ad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7820bdb1-a61e-433c-9df2-427d33357042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "358c71fb-669d-453c-93c0-a132e10cd488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b9c2f7cb-2572-4d6a-b179-551fffe674ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5e6d4bab-de7b-4a91-9dd5-b4ae9130dc1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "67d0214c-2ca2-4f5d-949e-f0c51f4dc6f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "edc73dad-2f0d-459a-b722-6d5b3460ac69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "aea1d573-a01b-48c1-8bd5-f7a73207164a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "eb866171-0228-4a40-991f-5965084355b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 20
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "997a9ef2-e184-4c66-950e-0fad8d8e0f9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 20
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f69ab526-e58f-488d-9a7b-3e01f47154ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 20
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "03e9fae9-fa47-4ace-bac3-cfd116b895e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 52,
                "y": 92
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "67a42052-43fd-47c7-a5d5-0a0d8c69f2d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 38
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f279e0a0-53f2-42de-a6df-f5917762dea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 46,
                "y": 92
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3fa3fe06-7e56-46b6-82ed-5169f893f2cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 38
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e5707fb7-a2aa-4285-86b5-6b645c858cb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 38
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e129e83c-f071-42f1-ac88-0527f2212da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 40,
                "y": 92
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "88856ffa-3b19-4cb5-8830-ab8ab385bc4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 38
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "73f73b19-728a-4eb5-a402-df55254f1a64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 38
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "18e58137-53f0-4e6f-91af-34a1f7694629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 38
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7a639994-dd15-4fdf-8d7e-c4f1ca3af932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "23270fae-3578-4e35-b7eb-f79a1f2596c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 20
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "90a9221b-d143-42db-a0c0-70db6364e3b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 20
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8002a9ee-7334-4cf3-b7f6-08c0ad5be8f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 20
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "aa941f84-b243-4e81-a804-14e99c567629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 20
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b1f06fd2-6e15-4cab-84bf-2813727feaed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 116,
                "y": 92
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "22caa31f-8bae-46f4-88fa-1fb12867ccfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 20
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2f62ea5a-27cb-4245-9605-42ec920488e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 20
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "243e5257-85fb-4c36-a213-26d90f40d515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 38
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "864edc42-5a5d-46d3-a586-cea414a99c45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 92
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "92d14a1d-c006-4fb5-b7ea-d5e0a30ce73a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 20
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "269f622e-a689-425d-a238-68347a8da431",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "dd9dfc4a-bf90-46e1-ad97-2f8ee5a4e2de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 38
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f114a7a7-0a93-48b8-be2a-9b831e9a954c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 20
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b95b5f1c-c584-476e-8033-4aad826b069b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 20
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "68a12dfc-59ee-495b-81cd-9569edc50882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 74
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f62019d0-aa49-4553-9667-d685717de1bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 74
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "460cab82-5850-4937-9665-5d7535dfdc1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 56
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2790ecd1-1853-4c25-bafe-36e37640fe8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f9c034e0-1ef7-4fb6-8f6b-52867f830b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 56
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a414d7c3-833c-4dfd-b825-f53f55f728f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 56
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7f6cfddd-eead-4cc2-87d3-76f35875d0d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 56
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "877a4430-212e-4e74-9580-2d92ba1738ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 56
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7f50a2fe-ef0a-4b15-9aeb-99c329f114b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 56
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a6cfa20f-3663-44e8-aeee-47b848d3c3c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 112,
                "y": 92
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "52d74d16-1291-457d-ae89-44d3bb9c5c72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3b0a8289-11dc-4e2e-9457-0e830ca0131f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 82,
                "y": 92
            }
        }
    ],
    "image": null,
    "includeTTF": true,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": null,
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}