{
    "id": "99604d07-13bf-4bf7-a2e8-00ec4cef7425",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d1c581e-5a1b-43f2-b3d4-3a9d7c05d62a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99604d07-13bf-4bf7-a2e8-00ec4cef7425",
            "compositeImage": {
                "id": "2436c639-e4fd-477d-aefd-00d2f0ae264d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d1c581e-5a1b-43f2-b3d4-3a9d7c05d62a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2602f40-0f20-40c1-982f-bb5574c103c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d1c581e-5a1b-43f2-b3d4-3a9d7c05d62a",
                    "LayerId": "cc767bc4-3036-4d56-890e-c3eaa6d4b7f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "cc767bc4-3036-4d56-890e-c3eaa6d4b7f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99604d07-13bf-4bf7-a2e8-00ec4cef7425",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}