{
    "id": "d383e6fc-1fc6-4783-900c-b88db5d2681e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCheck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "235d3ca3-4efd-459f-822e-7a31718f4ff8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d383e6fc-1fc6-4783-900c-b88db5d2681e",
            "compositeImage": {
                "id": "fe927c68-a4a3-4953-a80b-ec2d32d23643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "235d3ca3-4efd-459f-822e-7a31718f4ff8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feb3499a-679d-47d7-95c3-370b9e4b98a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "235d3ca3-4efd-459f-822e-7a31718f4ff8",
                    "LayerId": "56d9de1a-8322-4fb3-b538-3bf46c4744a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "56d9de1a-8322-4fb3-b538-3bf46c4744a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d383e6fc-1fc6-4783-900c-b88db5d2681e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}