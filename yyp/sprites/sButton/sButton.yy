{
    "id": "89d6e979-aa49-4180-8eee-a90675cc95e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5d97ef2-a527-472d-b294-2e93dab8be85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "compositeImage": {
                "id": "f20560cb-1b9f-4b92-8305-84905bcef159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d97ef2-a527-472d-b294-2e93dab8be85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6581d3c2-a5f4-438a-bfe4-527466528443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d97ef2-a527-472d-b294-2e93dab8be85",
                    "LayerId": "70fa1583-8530-4605-996f-3be6e13f96f0"
                }
            ]
        },
        {
            "id": "92db22a9-ab4e-4f16-8a19-b205d3439f4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "compositeImage": {
                "id": "c208ebb5-d30e-436e-845f-aa96757ac35e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92db22a9-ab4e-4f16-8a19-b205d3439f4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8c66567-96da-4c8a-bd2f-b0495596a3ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92db22a9-ab4e-4f16-8a19-b205d3439f4e",
                    "LayerId": "70fa1583-8530-4605-996f-3be6e13f96f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "70fa1583-8530-4605-996f-3be6e13f96f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}