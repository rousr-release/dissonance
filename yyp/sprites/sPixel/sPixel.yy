{
    "id": "909ae02d-c306-43b4-a1db-f06746d258b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba75cff4-5bc4-4c8d-abcd-3b1cab3bd40a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "909ae02d-c306-43b4-a1db-f06746d258b4",
            "compositeImage": {
                "id": "03b8aeb3-0b00-44ac-a999-9a931705a2ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba75cff4-5bc4-4c8d-abcd-3b1cab3bd40a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d582c41-45e3-458b-87b8-65116a8fe0ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba75cff4-5bc4-4c8d-abcd-3b1cab3bd40a",
                    "LayerId": "d5fee354-6660-479a-98b8-b0d25281a933"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "d5fee354-6660-479a-98b8-b0d25281a933",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "909ae02d-c306-43b4-a1db-f06746d258b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}