#include "rousrDissonance.h"

#include "GMLDSMap.h"

#include "Discord.h"

extern "C" {
	DLL_API double Init(const char* _applicationID, const char* _steamID) {
        Discord::Init(_applicationID, _steamID);
        
        Discord::SetOnReady([](const DiscordUser* user) {
            CGMLDSMap dsMap;
            dsMap.AddString("discord_event", "ready");
            dsMap.TriggerAsyncEvent();
        });

        Discord::SetOnError([](int _error_code, const char* _msg) {
            CGMLDSMap dsMap;
            dsMap.AddString("discord_event", "error");
            dsMap.AddDouble("error_code", static_cast<double>(_error_code));
            dsMap.AddString("msg", _msg);
            dsMap.TriggerAsyncEvent();
        });

        Discord::SetOnDisconnected([](int _error_code, const char* _msg) {
            CGMLDSMap dsMap;
            dsMap.AddString("discord_event", "disconnected");
            dsMap.AddDouble("error_code", static_cast<double>(_error_code));
            dsMap.AddString("msg", _msg);
            dsMap.TriggerAsyncEvent();
        });

        Discord::SetOnJoinGame([](const char* _join_secret) {
            CGMLDSMap dsMap;
            dsMap.AddString("discord_event", "join");
            dsMap.AddString("secret", _join_secret);
            dsMap.TriggerAsyncEvent();
        });
        
        Discord::SetOnSpectateGame([](const char* _spectate_secret) {
            CGMLDSMap dsMap;
            dsMap.AddString("discord_event", "spectate");
            dsMap.AddString("secret", _spectate_secret);
            dsMap.TriggerAsyncEvent();
        });

        Discord::SetOnJoinRequest([](const DiscordUser* _user) {
            CGMLDSMap dsMap;
            dsMap.AddString("discord_event", "join_request");
            dsMap.AddString("user_id", _user->userId);
            dsMap.AddString("user_name", _user->username);
            dsMap.AddString("avatar", _user->avatar);
            dsMap.TriggerAsyncEvent();
        });

        return 1.0;
	}    

    DLL_API double UpdatePresence() {
        Discord::UpdatePresence();
        return 1.0;
    }

    DLL_API double RunCallbacks() {
        Discord::RunCallbacks();
        return 1.0;
    }

    DLL_API double ResetPresence() {
        Discord::ResetPresence();
        return 1.0;
    }

    DLL_API double SetState(const char* _state) {
        Discord::SetState(_state);
        return 1.0;
    }

    DLL_API double SetDetails(const char* _details) {
        Discord::SetDetails(_details);
        return 1.0;
    }

    DLL_API double SetTimeStamps(double _startTimeStampLo, double _startTimeStampHi, double _endTimeStampLo, double _endTimeStampHi) {
        int64_t startTimeStamp = static_cast<int32_t>(_startTimeStampLo) + (static_cast<int32_t>(_startTimeStampHi) << 31);
        int64_t endTimeStamp   = static_cast<int32_t>(_endTimeStampLo) + (static_cast<int32_t>(_endTimeStampHi) << 31);

        Discord::SetTimeStamps(startTimeStamp, endTimeStamp);
        return 1.0;
    }

    DLL_API double SetLargeImage(const char* _largeImageKey, const char* _largeImageText) {
        Discord::SetLargeImage(_largeImageKey, _largeImageText);
        return 1.0;
    }

    DLL_API double SetSmallImage(const char* _smallImageKey, const char* _smallImageText) {
        Discord::SetSmallImage(_smallImageKey, _smallImageText);
        return 1.0;
    }

    DLL_API double SetPartyData(const char* _partyId, double _partySize, double _partyMax) {
        Discord::SetPartyData(_partyId, static_cast<int32_t>(_partySize), static_cast<int32_t>(_partyMax));
        return 1.0;
    }

    DLL_API double SetMatchSecret(const char* _matchSecret, double _instance) {
        Discord::SetMatchSecret(_matchSecret, static_cast<int32_t>(_instance));
        return 1.0;
    }

    DLL_API double SetJoinSecret(const char* _joinSecret) { 
        Discord::SetJoinSecret(_joinSecret);
        return 1.0;
    }

    DLL_API double SetSpectateSecret(const char* _spectateSecret) { 
        Discord::SetSpectateSecret(_spectateSecret); 
        return 1.0;
    }

    DLL_API double Respond(const char* _userId, double _reply) {
        Discord::Respond(_userId, static_cast<int>(_reply));
        return 1.0;
    }

    DLL_API double Shutdown() {
        Discord::Shutdown();
        return 1.0;
    }
}