#include "GMLDSMap.h"

#include "GMLVal.h"

bool                                   CGMLDSMap::mRegistered = false;
CGMLDSMap::FnCreateAsyncEventWithDSMap CGMLDSMap::CreateAsyncEventWithDSMap = nullptr;
CGMLDSMap::FnCreateDsMap               CGMLDSMap::CreateDSMap = nullptr;
CGMLDSMap::FnDsMapAddDouble            CGMLDSMap::DSMapAddDouble = nullptr;
CGMLDSMap::FnDsMapAddString            CGMLDSMap::DSMapAddString = nullptr;

void CGMLDSMap::AddDouble(const std::string& _key, double _val) {
	auto mapVal(std::make_tuple(std::make_shared<TGMLVal<std::string>>(_key), std::make_shared<TGMLVal<double>>(_val)));
	mMap[_key] = mapVal;
}

void CGMLDSMap::AddString(const std::string& _key, const std::string& _val) {
	auto mapVal(std::make_tuple(std::make_shared<TGMLVal<std::string>>(_key), std::make_shared<TGMLVal<std::string>>(_val)));
	mMap[_key] = mapVal;
}

void CGMLDSMap::TriggerAsyncEvent(int _event) {
	// more info: http://gamemaker.communitymanual.com/docs/asynchronous-functions/index

	// TODO: keep a list of stored strings that we clear on each async wipe.
	
	// After passing this back to GML, the ID is no longer valid, so we don't store it.
	int gmlId(CreateDSMap(0));
	for (auto itMap(mMap.begin()), itEnd(mMap.end()); itMap != itEnd; ++itMap) {
		auto member(itMap->second);
		
		char* name(std::get<0>(member)->Val());
		auto val(std::get<1>(member));
		
		val->WriteToMap(gmlId, name);
	}

	CreateAsyncEventWithDSMap(gmlId, _event);
}

void CGMLDSMap::RegisterCallbacks(char* _arg1, char* _arg2, char* _arg3, char* _arg4) {
	mRegistered = true;
	CreateAsyncEventWithDSMap = reinterpret_cast<FnCreateAsyncEventWithDSMap>(_arg1);
	CreateDSMap = reinterpret_cast<FnCreateDsMap>(_arg2);
	DSMapAddDouble = reinterpret_cast<FnDsMapAddDouble>(_arg3);
	DSMapAddString = reinterpret_cast<FnDsMapAddString>(_arg4);
}

extern "C" {

	// Note on function signature
	// https://stackoverflow.com/questions/40375949/gamemaker-studio-create-async-event-from-windows-dll
	DLL_API void RegisterCallbacks(char *_arg1, char * _arg2, char *_arg3, char *_arg4)
	{
		CGMLDSMap::RegisterCallbacks(_arg1, _arg2, _arg3, _arg4);
	}
}