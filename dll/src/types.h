#pragma once

#include "dllapi.h"

////
// C++
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

////
// Platform
#ifdef WIN32
#include <Windows.h>
#endif 

////
// STL
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <queue>
#include <set>
#include <unordered_set>
#include <stack>

#include <tuple>
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <atomic>

////
// Constants

#define MaxGMLStringLen (1024)
#define size_t_max      (static_cast<size_t>(~0))

#if defined (__GNUC__)
#define strncpy_s(a, b, c) strncpy(a, b, c)
#endif 
