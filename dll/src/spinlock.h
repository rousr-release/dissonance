#pragma once

#include <atomic>

////
// Spinlock
// courtesy: anki3d.org/spinlock
class SpinLock {
public:
    void lock() {
        while (mLock.test_and_set(std::memory_order_acquire)) { ; }
    }

    void unlock() {
        mLock.clear(std::memory_order_release);
    }

private:
    std::atomic_flag mLock = ATOMIC_FLAG_INIT;
};