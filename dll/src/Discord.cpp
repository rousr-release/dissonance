#include "rousrDissonance.h"

#include "Discord.h"

#include "../discord-rpc/discord_register.h"

#if defined(_MSC_VER)
#pragma comment(lib, "discord-rpc/discord-rpc.lib")
#endif 

std::unique_ptr<Discord> Discord::mInstance(nullptr);

void Discord::Init(const std::string& _applicationID, const std::string& _steamID)
{
    mInstance.reset(new Discord(_applicationID, _steamID));
}

void Discord::Shutdown()
{
    mInstance.reset();
}


Discord::Discord(const std::string& _applicationID, const std::string& _steamID)
    : mAppID(_applicationID)
    , mSteamID(_steamID)
{
    DiscordEventHandlers handlers;
    
    std::memset(&mPresence, 0, sizeof(mPresence));
    std::memset(&handlers, 0, sizeof(handlers));
    
    handlers.ready        = &Discord::HandleReady;
    handlers.disconnected = &Discord::HandleDisconnected;
    handlers.errored      = &Discord::HandleError;
    
    handlers.joinGame     = &Discord::HandleJoinGame;
    handlers.spectateGame = &Discord::HandleSpectateGame;
    handlers.joinRequest  = &Discord::HandleJoinRequest;

    Discord_Initialize(mAppID.c_str(), &handlers, 1, mSteamID.empty() || mSteamID.size() == 0 ? nullptr : mSteamID.c_str());   

    // Discord_Register(const char* applicationId, const char* command);
    // Discord_RegisterSteamGame(const char* applicationId, const char* steamId);
    std::string command("");
#if defined WIN32
    wchar_t path[MAX_PATH];
    GetModuleFileName(0, path, MAX_PATH);

    std::wstring ws(path);
    command = std::string(ws.begin(), ws.end());
#endif
    
    Discord_Register(mAppID.c_str(), command.c_str());
}

Discord::~Discord() {
    Discord_Shutdown();
    mInstance.reset(nullptr);
}

void Discord::SetState(const std::string& _state)
{
    if (mInstance != nullptr) {
        mInstance->mState = _state;

        mInstance->mPresence.state = _state.empty() ? nullptr : mInstance->mState.c_str();
    }
}

void Discord::SetDetails(const std::string& _details)
{
    if (mInstance) {
        mInstance->mDetails = _details;
        
        mInstance->mPresence.details = _details.empty() ? nullptr : mInstance->mDetails.c_str();
    }
}

void Discord::SetLargeImage(const std::string& _largeImageKey, const std::string& _largeImageText)
{
    if (mInstance) {
        mInstance->mLargeImageKey  = _largeImageKey;
        mInstance->mLargeImageText = _largeImageText;

        mInstance->mPresence.largeImageKey  = _largeImageKey.empty() ? nullptr : mInstance->mLargeImageKey.c_str();
        mInstance->mPresence.largeImageText = _largeImageText.empty() ? nullptr : mInstance->mLargeImageText.c_str();
    }
}

void Discord::SetSmallImage(const std::string& _smallImageKey, const std::string& _smallImageText)
{
    if (mInstance) {
        mInstance->mSmallImageKey = _smallImageKey;
        mInstance->mSmallImageText = _smallImageText;

        mInstance->mPresence.smallImageKey = _smallImageKey.empty() ? nullptr : mInstance->mSmallImageKey.c_str();
        mInstance->mPresence.smallImageText = _smallImageText.empty() ? nullptr : mInstance->mSmallImageText.c_str();
    }

}
void Discord::SetPartyData(const std::string& _partyId, int32_t _partySize, int32_t _partyMax)
{
    if (mInstance != nullptr) {
        mInstance->mPartyID = _partyId;

        mInstance->mPresence.partyId = _partyId.empty() ? nullptr : mInstance->mPartyID.c_str();
        mInstance->mPresence.partySize = _partyId.empty() ? 0 : _partySize;
        mInstance->mPresence.partyMax = _partyId.empty() ? 0 : _partyMax;
    }
}

void Discord::SetMatchSecret(const std::string& _matchSecret, int32_t _instance) {
    if (mInstance) {
        mInstance->mMatchSecret = _matchSecret;

        mInstance->mPresence.matchSecret = _matchSecret.empty() ? nullptr : mInstance->mMatchSecret.c_str();
        mInstance->mPresence.instance = _matchSecret.empty() ? 0 : _instance;
    }
}

void Discord::SetJoinSecret(const std::string& _joinSecret)
{
    if (mInstance != nullptr) {
        mInstance->mJoinSecret = _joinSecret;

        mInstance->mPresence.joinSecret = _joinSecret.empty() ? nullptr : mInstance->mJoinSecret.c_str();
    }
}

void Discord::SetSpectateSecret(const std::string& _spectateSecret)
{
    if (mInstance != nullptr) {
        mInstance->mSpectateSecret = _spectateSecret;

        mInstance->mPresence.spectateSecret = _spectateSecret.empty() ? nullptr : mInstance->mSpectateSecret.c_str();
    }
}

void Discord::ResetPresence()
{
    if (mInstance != nullptr) {
        std::memset(&mInstance->mPresence, 0, sizeof(mInstance->mPresence));
        
        mInstance->mState.clear();
        mInstance->mDetails.clear();
        
        mInstance->mLargeImageKey.clear(); 
        mInstance->mLargeImageText.clear();
        mInstance->mSmallImageKey.clear();
        mInstance->mSmallImageText.clear();
        mInstance->mPartyID.clear();
        mInstance->mUserID.clear();

        mInstance->mMatchSecret.clear();
        mInstance->mJoinSecret.clear();
        mInstance->mSpectateSecret.clear();
    }
}

void Discord::SetTimeStamps(int64_t _startTimeStamp, int64_t _endTimeStamp)
{
    if (mInstance != nullptr) {
        mInstance->mPresence.startTimestamp = _startTimeStamp;
        mInstance->mPresence.endTimestamp   = _endTimeStamp;
    }
}

void Discord::UpdatePresence()
{
    if (mInstance != nullptr)
        Discord_UpdatePresence(&mInstance->mPresence);
}

void Discord::Respond(const std::string& _userId, int _response)
{
    if (mInstance != nullptr) {
        mInstance->mUserID = _userId;
        Discord_Respond(mInstance->mUserID.c_str(), _response);
    }
}

void Discord::HandleReady(const DiscordUser* user) {
    if (mInstance != nullptr && mInstance->mHandleReady != nullptr)
        mInstance->mHandleReady(user);
}

void Discord::HandleDisconnected(int _errorCode, const char* _message) {
    if (mInstance != nullptr && mInstance->mHandleDisconnected != nullptr)
        mInstance->mHandleDisconnected(_errorCode, _message);
}

void Discord::HandleError(int _errorCode, const char* _message) {
    if (mInstance != nullptr && mInstance->mHandleError != nullptr)
        mInstance->mHandleError(_errorCode, _message);
}

void Discord::HandleJoinGame(const char* _joinSecret) {
    if (mInstance != nullptr && mInstance->mHandleJoinGame != nullptr)
        mInstance->mHandleJoinGame(_joinSecret);
}

void Discord::HandleSpectateGame(const char* _spectateSecret) {
    if (mInstance != nullptr && mInstance->mHandleSpectateGame != nullptr)
        mInstance->mHandleSpectateGame(_spectateSecret);
}

void Discord::HandleJoinRequest(const DiscordUser* _user) {
    if (mInstance != nullptr && mInstance->mHandleJoinRequest != nullptr)
        mInstance->mHandleJoinRequest(_user);
}