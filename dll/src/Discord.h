#pragma once

#include "../discord-rpc/discord_rpc.h"

class Discord
{
public:
    static void Init(const std::string& _applicationID, const std::string& _steamID);
    static void Shutdown();

    virtual ~Discord();

    static void SetOnReady(std::function<void(const DiscordUser*)> _ready) { if (mInstance != nullptr) mInstance->mHandleReady = _ready; }
    static void SetOnDisconnected(std::function<void(int, const char*)> _disc) { if (mInstance != nullptr) mInstance->mHandleDisconnected = _disc; }
    static void SetOnError(std::function<void(int, const char*)> _error) { if (mInstance != nullptr) mInstance->mHandleError = _error; }
    static void SetOnJoinGame(std::function<void(const char*)> _join) { if (mInstance != nullptr) mInstance->mHandleJoinGame = _join; }
    static void SetOnSpectateGame(std::function<void(const char*)> _spectate) { if (mInstance != nullptr) mInstance->mHandleSpectateGame = _spectate; }
    static void SetOnJoinRequest(std::function<void(const DiscordUser*)> _request) { if (mInstance != nullptr) mInstance->mHandleJoinRequest = _request; }

    static void SetState(const std::string& _state);
    static void SetDetails(const std::string& _details);
    static void SetTimeStamps(int64_t _startTimeStamp, int64_t _endTimeStamp);
    static void SetLargeImage(const std::string& _largeImageKey, const std::string& _largeImageText);
    static void SetSmallImage(const std::string& _smallImageKey, const std::string& _smallImageText);
    static void SetPartyData(const std::string& _partyId, int32_t _partySize, int32_t _partyMax);
    static void SetMatchSecret(const std::string& _matchSecret, int32_t _instance);
    static void SetJoinSecret(const std::string& _matchSecret);
    static void SetSpectateSecret(const std::string& _spectateSecret);

    static void ResetPresence();
    static void UpdatePresence();

    // Call once per step?
    static void RunCallbacks() { Discord_RunCallbacks(); }
    static void Respond(const std::string& _userId, int _response);

private:
    Discord(const std::string& _applicationID, const std::string& _steamID);

    static std::unique_ptr<Discord> mInstance;

    static void HandleReady(const DiscordUser* _user);
    static void HandleDisconnected(int _errorCode, const char* _message);
    static void HandleError(int _errorCode, const char* _message);
    static void HandleJoinGame(const char* _joinSecret);
    static void HandleSpectateGame(const char* _spectateSecret);
    static void HandleJoinRequest(const DiscordUser* user);

private:
    DiscordRichPresence mPresence;

    // Discord Init
    std::string mAppID;
    std::string mSteamID;
    
    std::string mState;
    std::string mDetails;

    // image
    std::string mSmallImageKey;
    std::string mSmallImageText;
    std::string mLargeImageKey;
    std::string mLargeImageText;

    std::string mPartyID;
    std::string mUserID;

    // shhhhhhhh, they're secret.
    std::string mMatchSecret;
    std::string mJoinSecret;
    std::string mSpectateSecret;

private:
    // Handlers
    std::function<void(const DiscordUser*)>        mHandleReady        = nullptr;
    std::function<void(int, const char*)>          mHandleDisconnected = nullptr;
    std::function<void(int, const char*)>          mHandleError        = nullptr;
    std::function<void(const char*)>               mHandleJoinGame     = nullptr;
    std::function<void(const char*)>               mHandleSpectateGame = nullptr;
    std::function<void(const DiscordUser*)>        mHandleJoinRequest  = nullptr;

public:
    Discord() = delete;
    Discord(const Discord&) = delete;
    Discord(const Discord&&) = delete;
    Discord& operator=(const Discord&) = delete;
};