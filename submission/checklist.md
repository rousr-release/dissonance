## Submission Checklist

*   update README.md
*   rousrDissonance.yyp
    *   increment version number in extension
    *   update README.gml
    *   update included README
    *   add asset and upload to marketplace
        *   update release ntoes
        *   publish
    *   build new yyz
    *   [optional] build demo if major feature change
        *   upload to gitlab
            *   fix link in dissonance.rou.sr
            *   update itch.io
            *   update yyg marketplace forum link
*   itch.io: update release notes (https://itch.io/game/edit/193844)
*   gmc - marketplace: update release notes (https://forum.yoyogames.com/index.php?threads/dissonance-discord-rich-presence.37799/)
*   dissonance.rou.sr: update release notes